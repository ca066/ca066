#include<stdio.h>
void compute(int *,int *,int *);
int main()
{
    int n1,n2,gcd;
    printf("enter the first number:\n");
    scanf("%d",&n1);
    printf("enter the second number:\n");
    scanf("%d",&n2);
    compute(&n1,&n2,&gcd);
    printf("the gcd of %d and %d is %d\n",n1,n2,gcd);
    return 0;
}
void compute(int *n1,int *n2,int *gcd)
{
    int i;
    for(i=1;i<=*n1 && i<=*n2;i++)
    {
        if(*n1%i==0 && *n2%i==0)
        {
            *gcd=i;
        }
    }
}