#include<stdio.h>
int main()
{
    int a,b,sum,diff,mult,divi,rem,*ap,*bp,*sump,*diffp,*multp,*divip,*remp;
    ap=&a;
    bp=&b;
    sump=&sum;
    diffp=&diff;
    multp=&mult;
    divip=&divi;
    remp=&rem;
    printf("enter the first number:\n");
    scanf("%d",&a);
    printf("enter the second number:\n");
    scanf("%d",&b);
    *sump=*ap+*bp;
    *diffp=*ap-*bp;
    *multp=(*ap)*(*bp);
    *divip=(*ap)/(*bp);
    *remp=(*ap)%(*bp);
    printf("the sum is %d\n",*sump);
    printf("the difference is %d\n",*diffp);
    printf("the product is %d\n",*multp);
    printf("the quotient is %d\n",*divip);
    printf("the remainder is %d\n",*remp);
    return 0;
}
//write your code here