#include <stdio.h>
void swap(int *,int *);
void display(int a,int b);
int main()
{
    int a,b;
    printf("Enter the first number:\n");
    scanf("%d",&a);
    printf("Enter the second number:\n");
    scanf("%d",&b);
    printf("before swapping %d\t%d\t\n",a,b);
    swap(&a,&b);
    display(a,b);
    return 0;
}
void swap(int *x,int *y)
{
    int temp;
    temp=*x;
    *x=*y;
    *y=temp;
}
void display(int a,int b)
{
    printf("after swapping %d\t%d\t\n",a,b);
}//write your code here
