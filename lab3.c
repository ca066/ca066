#include<stdio.h>
#include<math.h>
void ans(int a,int b,int c);
int main()
{
	int a,b,c;
	printf("if the quadratic equation is of type ax^2+bx+c=0 enter values of a b c\n");
	scanf("%d %d %d",&a,&b,&c);
	ans(a,b,c);
	return 0;
}
void ans(int a,int b,int c)
{
	float root1,root2,d;
	d=b*b-4*a*c;
	if(d>0)
	{
	root1=(-b+sqrt(d))/(2*a);
	root2=(-b-sqrt(d))/(2*a);
	printf("the solution of the quadratic equation is %f and %f\n",root1,root2);
    }
   else if(d==0)
     {
	root1=-b/(2*a);
	root2=-b/(2*a);
	printf("the solution of the quadratic equation is %f and %f\n",root1,root2);
    }
else
{
	printf("the roots are imaginary\n");
}
}