//write your code here
#include<stdio.h>
int main()
{
    typedef struct 
    {
        int dd;
        int mm;
        int yy;
    }date;
    struct details
    {
        int empid;
        char name[100];
        date dob;
        double sal;
        char desig[100];
        float exper;
    }det;
    printf("enter employee id;\n");
    scanf("%d",&det.empid);
    printf("enter name:\n");
    scanf("%s",&det.name);
    printf("enter dob in dd mm yy format\n");
    scanf("%d%d%d",&det.dob.dd,&det.dob.mm,&det.dob.yy);
    printf("enter salary:\n");
    scanf("%lf",&det.sal);
    printf("enter designation:\n");
    scanf("%s",&det.desig);
    printf("enter experience:\n");
    scanf("%f",&det.exper);
    printf("the employee details are\n");
    printf("employee id:%d\n",det.empid);
    printf("name:%s\n",det.name);
    printf("dob:%d/%d/%d\n",det.dob.dd,det.dob.mm,det.dob.yy);
    printf("salary:%lf\n",det.sal);
    printf("designation:%s\n",det.desig);
    printf("experience:%f\n",det.exper);
    return 0;
}